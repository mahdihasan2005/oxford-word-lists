# Oxford Word Lists

This repository contains oxford word lists from https://www.oxfordlearnersdictionaries.com/wordlists/ and the word lists are saved in a file format(.apkg) that you can import to Anki.

## Get the files

Clone the repository first.

```shell
git clone https://gitlab.com/mahdihasan2005/oxford-word-lists.git
```

Use files from `Exports` directory.
