import os
from gtts import gTTS
import sys

while True:
    def check_file_existence(directory, filename):
        filename += '.mp3'
        file_path = os.path.join(directory, filename)
        if os.path.exists(file_path):
            return True
        else:
            return False

    def generate_audio(directory, filename):
        tts = gTTS(filename)
        audio_file_name = filename + '.mp3'
        tts.save(f'{directory}/{audio_file_name}')

    # Example usage:
    directory = '/home/mahdi/.var/app/net.ankiweb.Anki/data/Anki2/Test 2/collection.media'
    filename = input("Word: ")
    file_exist = check_file_existence(directory, filename)
    if file_exist:
        print(f"\033[93m[{filename}.mp3] already exists!\033[0m")
    else:
        generate_audio(directory, filename)
        print(f"\033[92m[{filename}.mp3] added!\033[0m")
