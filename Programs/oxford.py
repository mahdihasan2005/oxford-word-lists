
#    ___ _   _  ____ ___  __  __ ____  _     _____ _____ _____ 
#   |_ _| \ | |/ ___/ _ \|  \/  |  _ \| |   | ____|_   _| ____|
#    | ||  \| | |  | | | | |\/| | |_) | |   |  _|   | | |  _|  
#    | || |\  | |__| |_| | |  | |  __/| |___| |___  | | | |___ 
#   |___|_| \_|\____\___/|_|  |_|_|   |_____|_____| |_| |_____|

import requests

app_id = 'your_app_id'
app_key = 'your_app_key'
language = 'en'
base_url = 'https://od-api.oxforddictionaries.com/api/v2/entries'

word = 'example'  # The word you want to search

url = f"{base_url}/{language}/{word.lower()}"
headers = {'app_id': app_id, 'app_key': app_key}

response = requests.get(url, headers=headers)

if response.status_code == 200:
    data = response.json()
    # Process the data as per your requirement
    print(data)
else:
    print('Request failed with status code', response.status_code)
