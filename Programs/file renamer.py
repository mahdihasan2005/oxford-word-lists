import os

def rename_to_lowercase(directory):
    # Change to the target directory
    os.chdir(directory)

    # Iterate over all files in the directory
    for file in os.listdir():
        # Check if the file is a regular file
        if os.path.isfile(file):
            # Convert the file name to lowercase
            lowercase_name = file.lower()

            # Rename the file to lowercase
            os.rename(file, lowercase_name)
            print(f"Renamed {file} to {lowercase_name}")

# Provide the directory path where you want to rename the files
directory_path = '/home/mahdi/Documents/GitLab/oxford-word-lists/Media/Audio/c1'

# Call the function to rename files to lowercase
rename_to_lowercase(directory_path)
