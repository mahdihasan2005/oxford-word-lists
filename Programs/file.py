import os

file = open("input.txt", "r")
lines = file.readlines()
words = []
double_words = []

for line in lines:
    line = line.strip()
    
    if line in words:
        double_words.append(line)

    if line not in words:
        words.append(line)

file.close()
print(double_words)

file = open("output.txt", "a")
for word in words:
    file.write(f"{word}\n")
file.close()