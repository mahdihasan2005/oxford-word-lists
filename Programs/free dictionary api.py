import requests
import json

def get_definiiton(word, language_code="en"):
    base_url = "https://api.dictionaryapi.dev/api/v2/entries"

    url = f"{base_url}/{language_code}/{word}"
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        # Process the data as per your requirement
        return data
    else:
        return 'Request failed with status code' + response.status_code


print(json.dumps(get_definiiton("example"), indent=2))