import os
from gtts import gTTS
import sys

# File to read
file_path = 'file-input.txt'

# Create 'Audio' directory if it doesn't exist
if not os.path.exists('Audio'):
    os.mkdir('Audio')

# Open the text file
with open(file_path, 'r') as file:
    # Get the total number of lines in the file
    total_lines = sum(1 for line in file)
    file.seek(0)
    
    # Keep track of the current line number
    current_line = 1
    
    # Read each line of the file
    for line in file:
        # Calculate the percentage of completion
        percentage_complete = (current_line / total_lines) * 100
        
        # Create a progress bar string
        progress_bar = "[" + "#" * int(percentage_complete // 2) + "." * (50 - int(percentage_complete // 2)) + "]"
        
        # Erase the entire line before updating the output
        sys.stdout.write('\033[F\033[K')
        
        # Display the text of the current line and the percentage of the file that has been transferred into audio
        print(f'Processing line {current_line}: {line.strip()}')
        print(f'Progress {progress_bar} {percentage_complete:.2f}%')
        
        # Create a gTTS object for the current line
        tts = gTTS(line)
        
        # Save the audio file in the 'Audio' directory
        # with a name corresponding to the text line
        audio_file_name = line.strip().replace(' ', '_') + '.mp3'
        tts.save(f'Audio/{audio_file_name}')
        
        # Increment the current line number
        current_line += 1
